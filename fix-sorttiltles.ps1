$rootPath = "./pack"

$isSave = $args[0] -eq "save"

$files = Get-ChildItem -Path $rootPath -Filter "season.nfo" -Recurse

$debug = $false
$count = 0
$to_adds = @() 

foreach ($file in $files) {
    try {
        [xml]$xmlContent = [xml](Get-Content -Path $file.FullName -Encoding UTF8)
        
        $seasonNumber = $xmlContent.season.seasonnumber
        $currentSortTitle = $xmlContent.season.sorttitle

        if ($currentSortTitle -eq $seasonNumber -or
            $currentSortTitle -eq "Saison $seasonNumber" -or 
            $currentSortTitle -eq "Saison $("{0:D2}" -f [int]$seasonNumber)" -or 
            $currentSortTitle -eq "Saison $("{0:D3}" -f [int]$seasonNumber)" -or 
            $null -ne ($currentSortTitle -as [int])) {
            
            if ($debug) {
                Write-Host "Sorttitle=$currentSortTitle in $($file.FullName) is already correct"
            }
            continue
        }
        $paddedSeasonNumber = "Saison {0:D3}" -f [int]$seasonNumber
        
        if ($isSave) {
            $xmlContent.season.sorttitle = $paddedSeasonNumber

            $xmlWriterSettings = New-Object System.Xml.XmlWriterSettings
            $xmlWriterSettings.Indent = $true
            $xmlWriterSettings.Encoding = [System.Text.Encoding]::UTF8
            $xmlWriterSettings.OmitXmlDeclaration = $false

            $xmlWriter = [System.Xml.XmlWriter]::Create($file.FullName, $xmlWriterSettings)
            $xmlContent.WriteTo($xmlWriter)
            $xmlWriter.Close()

            $to_adds += $file.FullName
        }

        $count++
        Write-Host "Updated sorttitle in $($file.FullName -replace '^.*?\\pack\\', '') from $currentSortTitle to $paddedSeasonNumber"
    } catch {
        Write-Host "Failed to process $($file.FullName): $_"
    }
}

if ($isSave) {
    Write-Host "`nSaved $count files`n"
    if ($to_adds.Count -gt 0) {
        foreach ($to_add in $to_adds) {
            git add $to_add
        }
    }
} else {
    Write-Host "Would have saved $count files"
}